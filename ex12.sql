SELECT books.title
FROM editions JOIN books ON books.id = editions.book_id
WHERE editions.type = 'h' AND EXISTS (SELECT books.title
FROM editions JOIN books ON books.id = editions.book_id
WHERE editions.type = 'p');