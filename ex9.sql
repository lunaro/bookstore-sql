SELECT
  book_id,
  SUM(stock)
FROM 
  stock JOIN editions ON stock.isbn = editions.isbn
GROUP BY
  book_id
ORDER BY
  2 DESC