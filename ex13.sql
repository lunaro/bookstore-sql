SELECT
  publishers.name, ROUND(AVG(stock.retail),2), count(editions.book_id)
FROM
  (publishers JOIN editions ON publishers.id = publisher_id) JOIN stock ON stock.isbn = editions.isbn
GROUP BY
  publishers.name