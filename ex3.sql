SELECT 
  editions.isbn, 
  books.title,
  stock.retail,
  stock.stock
FROM 
  publishers, 
  editions, 
  books,
  stock
WHERE 
  editions.publisher_id = publishers.id AND
  books.id = editions.book_id AND
  stock.isbn = editions.isbn AND
  publishers.name = 'Random House'