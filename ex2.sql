SELECT 
  editions.isbn, 
  books.title
FROM 
  public.publishers, 
  public.editions, 
  public.books
WHERE 
  editions.publisher_id = publishers.id AND
  books.id = editions.book_id AND
  publishers.name = 'Random House';
