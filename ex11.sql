SELECT 
  authors.id, concat(first_name, ' ', last_name) as full_name, count(books.id)
FROM 
  authors, books
WHERE 
  author_id = authors.id
GROUP BY
  authors.id, first_name, last_name
ORDER BY
  3 DESC