SELECT
  ROUND(AVG(cost), 2) as "Average cost",
  ROUND(AVG(retail), 2) as "Average Retail",
  ROUND(AVG(retail - cost), 2) as "Average Profit"
FROM
  stock